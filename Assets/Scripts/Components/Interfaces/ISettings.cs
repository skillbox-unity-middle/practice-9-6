﻿public interface ISettings
{
    public int HeroHealth { get; }
    public int RecoverHealth { get; }
}

