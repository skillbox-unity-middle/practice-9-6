using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBalance : MonoBehaviour, ISettings
{
    [SerializeField] Settings _settings;

    public int HeroHealth => _settings.heroHealth;
    public int RecoverHealth => _settings.recoverHealth;
}
