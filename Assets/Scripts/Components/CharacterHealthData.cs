﻿using Unity.Entities;

public struct CharacterHealthData : IComponentData
{
    public int health;
}